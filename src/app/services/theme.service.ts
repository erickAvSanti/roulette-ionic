import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private toggleTheme: HTMLInputElement;

  constructor(
    private dataService: DataService,
  ) {

    this.startDefaultProps();
    this.toggleTheme = document.getElementById('toggleTheme') as HTMLInputElement;

    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    //checkToggle(prefersDark.matches);
    // Listen for changes to the prefers-color-scheme media query
    fromEvent(prefersDark, 'change').subscribe(
      (e: any) => {
        console.log(e);
        this.checkToggle(e.matches);
      }
    );
    prefersDark.addListener(e => {
      console.log(e);
      this.checkToggle(e.matches);
    });
  }
  // Called by the media query to check/uncheck the toggle
  checkToggle(shouldCheck: boolean) {
    this.toggleTheme.checked = shouldCheck;
  }
  toggleThemeChange(evt: CustomEvent): void {
    this.dataService.darkModeEnabled = evt.detail.checked;
    document.body.classList.remove('dark');
    if(evt.detail.checked){
      document.body.classList.add('dark');
    }
  }
  startDefaultProps(): void {
    this.toggleThemeChange({detail: {checked: this.dataService.darkModeEnabled}} as CustomEvent);
  }
}
