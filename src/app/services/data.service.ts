import { Injectable } from '@angular/core';
import { timer } from 'rxjs';
import { Roulette } from '../interfaces/roulette';
import { RouletteOption } from '../interfaces/roulette-option';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor() { }
  get roulettes(): Array<Roulette> {
    return JSON.parse(localStorage.getItem('roulettes')) || new Array<Roulette>();
  }
  set roulettes(values: Array<Roulette>) {
    localStorage.setItem('roulettes', JSON.stringify(values));
  }
  getRandomId(): number{
    const end = 100000000;
    const start = 100;
    return +(Math.random() * ( end - start) + start).toFixed(0);
  }

  getRoulette(id: number): Roulette {
    return this.roulettes.find( e => e.id === id);
  }

  addRoulette(title: string): Roulette {
    const roulettes = this.roulettes;
    const roulette = { id: this.getRandomId(), title, options: [] };
    roulettes.push(roulette);
    this.roulettes = roulettes;
    return roulette;
  }

  editRoulette(obj: Roulette): Roulette {
    const roulettes = this.roulettes;
    const roulette = roulettes.find( e => e.id === obj.id);
    if ( roulette ) {
      roulette.title = obj.title;
      roulette.options = obj.options;
      this.roulettes = roulettes;
      return roulette;
    }
    return null;

  }

  deleteRoulette(id: number): Roulette {
    const roulettes = this.roulettes;
    const roulette = roulettes.find(e => e.id === id);
    this.roulettes = roulettes.filter( e => e.id !== id );
    return roulette;
  }

  addRouletteOption(id: number, option: RouletteOption): RouletteOption {
    const roulettes = this.roulettes;
    const roulette = roulettes.find( e => e.id === id );
    if ( roulette ) {
      if(!option.id) {option.id = this.getRandomId();}
      roulette.options.push(option);
      this.roulettes = roulettes;
      return option;
    }
    return null;
  }

  editRouletteOption(id: number, option: RouletteOption): RouletteOption {
    const roulettes = this.roulettes;
    const roulette = roulettes.find( e => e.id === id );
    if ( roulette ) {
      const rouletteOption = roulette.options.find( e => e.id === option.id );
      if ( rouletteOption ) {
        rouletteOption.title = option.title;
        rouletteOption.color = option.color;
        this.roulettes = roulettes;
        return rouletteOption;
      }
    }
    return null;
  }

  deleteRouletteOption(id: number, optionId: number): RouletteOption {
    const roulettes = this.roulettes;
    const roulette = roulettes.find( e => e.id === id );
    if ( roulette ) {
      const rouletteOption = roulette.options.find(e => e.id === optionId);
      roulette.options = roulette.options.filter( e => e.id !== optionId );
      this.roulettes = roulettes;
      if( rouletteOption ) {
        return rouletteOption;
      }
    }
    return null;
  }

  setRouletteOptions(id: number, options: Array<RouletteOption>): void {
    const roulettes = this.roulettes;
    const roulette = roulettes.find( e => e.id === id );
    if ( roulette ) {
      roulette.options = options;
      this.roulettes = roulettes;
    }

  }

  set rouletteRangeIntervalTime(data: {lower: number; upper: number}){
    localStorage.setItem('rouletteRangeIntervalTime', JSON.stringify(data));
  }
  get rouletteRangeIntervalTime(): {lower: number; upper: number}{
    try {
      return JSON.parse(localStorage.getItem('rouletteRangeIntervalTime'));
    } catch (error) {}
    return null;
  }

  set rouletteInterleavedColors(data: Array<string>){
    localStorage.setItem('rouletteInterleavedColors', JSON.stringify(data));
  }
  get rouletteInterleavedColors(): Array<string>{
    try {
      return JSON.parse(localStorage.getItem('rouletteInterleavedColors'));
    } catch (error) {}
    return null;
  }
  set rouletteUseInterleavedColors(data: boolean){
    localStorage.setItem('rouletteUseInterleavedColors', `${data ? 1 : 0}`);
  }
  get rouletteUseInterleavedColors(): boolean{
    return +localStorage.getItem('rouletteUseInterleavedColors') === 1;
  }
  set rouletteArcColor(data: Array<number>){
    localStorage.setItem('rouletteArcColor', JSON.stringify(data));
  }
  get rouletteArcColor(): Array<number>{
    try {
      return JSON.parse(localStorage.getItem('rouletteArcColor'));
    } catch (error) {}
    return null;
  }

  set darkModeEnabled(data: boolean) {
    localStorage.setItem('darkModeEnabled', `${data ? 1 : 0}`);
  }
  get darkModeEnabled(): boolean {
    return +localStorage.getItem('darkModeEnabled') === 1;
  }
}
