import { Injectable } from '@angular/core';

declare let navigator: {language: ''; globalization: any};

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private lang = 'en';

  constructor() {
    if (window.Intl && typeof window.Intl === 'object') {
      //console.log('navigator.language', navigator.language);
      this.init(navigator.language);
    }
    else {
      navigator.globalization.getPreferredLanguage( (language) => {
        //console.log('language.value', language.value);
        this.init(language.value);
      }, () => {
      });
    }
  }
  init(language: string): void {
    if(language){
      try {
        this.lang = language.split('-')[0];
      } catch (ex) {
        this.lang = 'en';
      }
    }
  }
  getText(text: string): string {
    text = text.replace(/\s+/ig, ' ').trim();
    if(this.lang === 'es'){
      switch (text) {
        case 'Configuration':
          text = 'Configuración';
          break;
        case 'Toggle Dark Theme':
          text = 'Alternar Modo Oscuro';
          break;
        case 'Roulettes':
          text = 'Ruletas';
          break;
        case 'Roulette':
          text = 'Ruleta';
          break;
        case 'New Roulette':
          text = 'Nueva Ruleta';
          break;
        case 'Options':
          text = 'Opciones';
          break;
        case 'Title':
          text = 'Título';
          break;
        case 'Cancel':
          text = 'Cancelar';
          break;
        case 'Confirm':
          text = 'Confirmar';
          break;
        case 'Save changes':
          text = 'Guardar cambios';
          break;
        case 'Apply changes':
          text = 'Aplicar cambios';
          break;
        case 'Alert':
          text = 'Alerta';
          break;
        case 'Delete':
          text = 'Eliminar';
          break;
        case 'You can not revert this action':
          text = 'No puedes revertir esta acción';
          break;
        case 'Add Option':
          text = 'Agregar Opción';
          break;
        case 'Edit Option':
          text = 'Editar Opción';
          break;
        case 'Option title':
          text = 'Opción título';
          break;
        case 'Pick a color':
          text = 'Elige un color';
          break;
        case 'sec':
          text = 'seg';
          break;
        case 'Options':
          text = 'Opciones';
          break;
        case 'Adjust Period Time':
          text = 'Ajustar Periodo de Tiempo';
          break;
        case 'Use Interleaved Colors':
          text = 'Usar colores intercalados';
          break;
        case 'Select Interleaved Color':
          text = 'Seleccionar Color Intercalado';
          break;
        case 'Select Arc Color':
          text = 'Seleccionar Color del Arco';
          break;
        default:
          break;
      }
    }
    return text;
  }
}
