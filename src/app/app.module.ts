import { NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, Platform } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeService } from './services/theme.service';
import { Admob, AdmobOptions } from '@ionic-native/admob';
import { LanguageService } from './services/language.service';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    private themeService: ThemeService,
    private platform: Platform,
    private languageService: LanguageService,
  ){

    if(this.platform.is('android') && Admob?.AD_SIZE?.BANNER && false){
      const admobOptions: AdmobOptions = {
        bannerAdId: 'ca-app-pub-3940256099942544/6300978111',
        // interstitialAdId: 'XXX-XXXX-XXXX',
        // rewardedAdId: 'XXX-XXXX-XXXX',
        isTesting: true,
        autoShowBanner: false,
        autoShowInterstitial: false,
        autoShowRewarded: false,
        adSize: Admob.AD_SIZE.BANNER
      };
      // Set admob options
      Admob.setOptions(admobOptions)
        .then(() => console.log('Admob options have been successfully set'))
        .catch(err => console.error('Error setting admob options:', err));
      Admob.createBannerView()
        .then(() => console.log('Banner ad loaded'))
        .catch(err => console.error('Error loading banner ad:', err));
    }
  }
}
