import { AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Platform } from '@ionic/angular';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent implements OnInit, AfterViewInit {
  @ViewChild('stripCanvasEl', {read: ElementRef, static: true}) stripCanvasRef: ElementRef;
  @ViewChild('blockCanvasEl', {read: ElementRef, static: true}) blockCanvasRef: ElementRef;

  @Output() selectedColor = new EventEmitter<any>();
  @Output() selectedStripColor = new EventEmitter<any>();
  @Output() selectedBlockColor = new EventEmitter<any>();

  private defCanvasHeight = 30;

  private testColor = 'rgb(120, 200, 47)';

  constructor(
    public platform: Platform,
    private elRef: ElementRef,
  ) {
    this.platform.ready().then(
      (_) => {
        this.platform.resize.subscribe(
          (__) => {
            const el: HTMLElement = this.elRef.nativeElement;

            const stripCanvas: HTMLCanvasElement = this.stripCanvasRef.nativeElement;
            stripCanvas.width = el.clientWidth;
            stripCanvas.height = this.defCanvasHeight;
            this.drawStripGradient();

            const blockCanvas: HTMLCanvasElement = this.blockCanvasRef.nativeElement;
            blockCanvas.width = el.clientWidth;
            blockCanvas.height = 3 * this.defCanvasHeight;
            this.drawBlockGradient();
          }
        );
      },

    );
  }
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {

    this.selectedStripColor.subscribe(
      colorData => {
        this.testColor = `rgb(${colorData[0]}, ${colorData[1]}, ${colorData[2]})`;
        this.drawBlockGradient();
        this.drawStripGradient();
      }
    );

  }
  drawUI(): void {

    const stripCanvas: HTMLCanvasElement = this.stripCanvasRef.nativeElement;
    const stripCtx = stripCanvas.getContext('2d');

    const el: HTMLElement = this.elRef.nativeElement;
    stripCanvas.width = el.clientWidth;
    stripCanvas.height = this.defCanvasHeight;


    const blockCanvas: HTMLCanvasElement = this.blockCanvasRef.nativeElement;
    const blockCtx = blockCanvas.getContext('2d');
    blockCanvas.width = el.clientWidth;
    blockCanvas.height = 3 * this.defCanvasHeight;


    this.drawStripGradient();
    this.drawBlockGradient();

    fromEvent(stripCanvas, 'click').subscribe(
      (evt: PointerEvent) => {
        const imgData: ImageData = stripCtx.getImageData(evt.offsetX, evt.offsetY, 1 , 1);
        this.selectedColor.emit(imgData.data);
        this.selectedStripColor.emit(imgData.data);
      }
    );
    fromEvent(stripCanvas, 'touchmove').subscribe(
      (evt: TouchEvent) => {
        const cTouches = evt.changedTouches;
        const firstTouch = cTouches[0];
        const rect: DOMRect = stripCanvas.getBoundingClientRect();
        const offsetY = firstTouch.clientY - rect.y;
        const imgData: ImageData = stripCtx.getImageData(firstTouch.clientX, offsetY, 1 , 1);
        this.selectedColor.emit(imgData.data);
        this.selectedStripColor.emit(imgData.data);
      }
    );

    fromEvent(blockCanvas, 'click').subscribe(
      (evt: PointerEvent) => {
        const imgData: ImageData = blockCtx.getImageData(evt.offsetX, evt.offsetY, 1 , 1);
        this.selectedColor.emit(imgData.data);
        this.selectedBlockColor.emit(imgData.data);
      }
    );
    fromEvent(blockCanvas, 'touchmove').subscribe(
      (evt: TouchEvent) => {
        const cTouches = evt.changedTouches;
        const firstTouch = cTouches[0];
        const rect: DOMRect = blockCanvas.getBoundingClientRect();
        const offsetY = firstTouch.clientY - rect.y;
        const imgData: ImageData = blockCtx.getImageData(firstTouch.clientX, offsetY, 1 , 1);
        this.selectedColor.emit(imgData.data);
        this.selectedBlockColor.emit(imgData.data);
      }
    );
  }

  drawBlockGradient(): void {
    const canvas: HTMLCanvasElement = this.blockCanvasRef.nativeElement;
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = this.testColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const grdWhite = ctx.createLinearGradient(0, 0, canvas.width, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
    ctx.fillStyle = grdWhite;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const grdBlack = ctx.createLinearGradient(0, 0, 0, canvas.height);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    ctx.fillStyle = grdBlack;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

  }

  drawStripGradient(): void {
    const canvas: HTMLCanvasElement = this.stripCanvasRef.nativeElement;
    const ctx = canvas.getContext('2d');

    const gradient = ctx.createLinearGradient(0, 0, canvas.width, 0);

    const colors = [
      'rgb(255, 0, 0)',
      'rgb(255, 0, 255)',
      'rgb(0, 0, 255)',
      'rgb(0, 255, 255)',
      'rgb(0, 255, 0)',
      'rgb(255, 255, 0)',
      'rgb(255, 0, 0)',
      //'rgb(255, 255, 255)',
      //'rgb(0, 0, 0)',
    ];

    colors.forEach( (color, idx) => {
      const factor = idx / ( colors.length - 1);
      gradient.addColorStop(factor, color);
    });
    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
  }

}
