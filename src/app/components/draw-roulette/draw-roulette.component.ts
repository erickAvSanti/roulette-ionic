import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Platform } from '@ionic/angular';
import { fromEvent, of, timer } from 'rxjs';
import { mergeAll, throttleTime } from 'rxjs/operators';
import { Point2D } from 'src/app/interfaces/point2d';
import { Roulette } from 'src/app/interfaces/roulette';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-draw-roulette',
  templateUrl: './draw-roulette.component.html',
  styleUrls: ['./draw-roulette.component.scss'],
})
export class DrawRouletteComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('canvas', {read: ElementRef}) canvasRef: ElementRef;
  @Input() roulette: Roulette;
  @Input() showSpinButton = true;
  @Input() useInterleavedColors = true;
  @Input() heightAuto = false;
  @Input() fillFrame = false;
  @Input() centered = false;
  @Input() rangeIntervalTime = {lower: 1, upper: 10};
  @Input() interleavedColors = ['rgb(229, 142, 60)', 'rgb(229, 209, 60)'];
  @Input() arcColor = [37, 115, 120, 0.9];

  @Output() spinTimeChange = new EventEmitter();
  @Output() spinTimeTotalChange = new EventEmitter();
  canvasStartAngle = 0;
  spinTimeout = null;

  spinAngleStart = 0;
  spinTime = 0;
  spinTimeTotal = 0;
  private hostWidth = 0;
  private hostHeight = 500;

  private canvasStartTouch?: Point2D;
  private canvasEndTouch?: Point2D;

  private pearlSrc = '../../../assets/pearl.png';
  private pearlImage: HTMLImageElement;

  private colorAnchor = '#D1A825';

  constructor(
    private elRef: ElementRef,
    private platform: Platform,
    private dataService: DataService,
  ) {
  }
  ngAfterViewInit(): void {
    console.log(this.elRef);
    this.useInterleavedColors = this.dataService.rouletteUseInterleavedColors;
    const interleavedColors = this.dataService.rouletteInterleavedColors;
    if(interleavedColors){
      this.interleavedColors = interleavedColors;
    }
    this.asynDraw();

    this.platform.ready().then(
      (_) => {

        this.platform.resize.subscribe(
          (__) => {
            this.asynDraw();
          }
        );
      },
    );

    this.elementEvents();
  }
  elementEvents(): void {
    const canvas = this.canvasRef.nativeElement;
    const evtTouchEnd = fromEvent(canvas, 'touchend');
    const evtTouchMove = fromEvent(canvas, 'touchmove');
    of(
      evtTouchMove
      .pipe(
        throttleTime(400),
      ), evtTouchEnd
    )
    .pipe(mergeAll())
    .subscribe(
      (evt: TouchEvent) => {
        const cTouches = evt.changedTouches;
        const firstTouch = cTouches[0];
        const rect: DOMRect = canvas.getBoundingClientRect();
        const offsetY = firstTouch.clientY - rect.y;
        const offsetX = firstTouch.clientX;

        if(evt.type === 'touchmove'){ this.canvasStartTouch = {x: offsetX, y: offsetY}; }
        if(evt.type === 'touchend'){ this.canvasEndTouch = {x: offsetX, y: offsetY}; }
        if(evt.type === 'touchmove') {console.log(`canvasStartTouch = {x: ${this.canvasStartTouch.x}, y: ${this.canvasStartTouch.y}}`);}
        if(evt.type === 'touchend') {console.log(`canvasEndTouch = {x: ${this.canvasEndTouch.x}, y: ${this.canvasEndTouch.y}}`);}

        if( evt.type === 'touchend' && this.canvasEndTouch?.x > 30 && this.canvasStartTouch?.x > 30 ) {
          this.stopRotateWheel();
          this.spin(this.canvasEndTouch.y - this.canvasStartTouch.y);
        }
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`draw roulette changes`, changes);
    if(changes?.roulette?.currentValue && !changes?.roulette?.firstChange){
      this.asynDraw();
    }else if(changes?.interleavedColors?.currentValue && !changes?.interleavedColors?.firstChange){
      this.asynDraw();
    }else if(changes?.fillFrame){
      this.asynDraw();
    }else if(changes?.centered){
      this.asynDraw();
    }
  }

  ngOnInit(): void {
    this.pearlImage = new Image();
    this.pearlImage.src = this.pearlSrc;

  }
  asynDraw(): void {
    timer(500).subscribe(
      () => {
        const el: HTMLElement = this.elRef.nativeElement;
        this.hostWidth = el.clientWidth;
        if(this.heightAuto){
          this.hostHeight = el.clientHeight;
        }
        this.draw();
      }
    );
  }
  draw(): void {

    if(!this.roulette){
      return;
    }

    const canvas: HTMLCanvasElement = this.canvasRef.nativeElement;
    canvas.width = this.hostWidth;
    canvas.height = this.hostHeight;

    const centerX = this.centered ? this.hostWidth / 2 : 0;
    const centerY = this.hostHeight / 2;

    const ctx = canvas.getContext('2d');
    ctx.imageSmoothingEnabled = true;
    ctx.clearRect(0, 0, canvas.width, this.hostHeight);

    // const gradient = ctx.createLinearGradient(0, 0, canvas.width, 0);
    // gradient.addColorStop(0, '#fff');
    // gradient.addColorStop(1,  '#000');
    // ctx.fillStyle = gradient;
    // ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.font = 'bold 20px sans-serif';


    const options = this.roulette.options;
    let maxTextWidth = 0;
    const optionsMeasuredTextWidths = [];
    for (const option of options) {
      const textWidth = ctx.measureText(option.title.toUpperCase()).width;
      optionsMeasuredTextWidths.push(textWidth);
      if( textWidth > maxTextWidth ) {maxTextWidth = textWidth;}
    }


    const arc = 2 * Math.PI / options.length;

    const paddingWidth = 2;
    const insideRadius = 1 + paddingWidth;
    const outsideRadius = insideRadius + maxTextWidth + paddingWidth + 70;
    const textRadius = (outsideRadius + insideRadius) / 2 + 10;

    ctx.save();
    if(this.fillFrame){
      const scaleX = ( this.hostWidth / ( (this.centered ? 2 * outsideRadius : outsideRadius) + 20 ) );
      const scaleY = ( this.hostHeight / ( (this.centered ? 2 * outsideRadius : outsideRadius) + 20 ) );
      const minScale = this.hostWidth < this.hostHeight ? scaleX : scaleY;
      ctx.scale(minScale, minScale);
      if(this.centered){
        ctx.translate(- centerX + this.hostWidth / ( 2 * minScale), - centerY + this.hostHeight / ( 2 * minScale));
      }else{
        ctx.translate(0, - centerY + this.hostHeight / ( 2 * minScale));
      }

    }

    const boardTopx = 0;
    const boardTopy = 0;
    const squareSize = 200;
    const totalBoxs = 20;
    ctx.save();
    ctx.translate(centerX - squareSize * totalBoxs / 2, centerY - squareSize * totalBoxs / 2);
    for(let i=0; i<totalBoxs; i++) {
      for(let j=0; j<totalBoxs; j++) {
        ctx.fillStyle = ((i+j)%2 === 0) ? '#B78F0F':'#EFC337';
        const xOffset = boardTopx + j*squareSize;
        const yOffset = boardTopy + i*squareSize;
        ctx.beginPath();
        ctx.fillRect(xOffset, yOffset, squareSize, squareSize);
      }
    }
    ctx.restore();

    if(this.useInterleavedColors){
      ctx.strokeStyle = 'rgb(0, 0, 0, 0.4)';
    }else{
      ctx.strokeStyle = 'rgb(255, 255, 255)';
    }
    ctx.lineWidth = 1;
    options.forEach( (option, idx) => {
      const angle = this.canvasStartAngle + idx * arc;
      if(this.useInterleavedColors){
        if( idx % 2 === 0 ) {
          ctx.fillStyle = this.interleavedColors[0];
        }else {
          ctx.fillStyle = this.interleavedColors[1];
        }
      }else{
        ctx.fillStyle = `rgb(${option.color.r}, ${option.color.g}, ${option.color.b})`;
      }
      ctx.beginPath();
      ctx.arc(centerX, centerY, outsideRadius, angle, angle + arc, false);
      ctx.arc(centerX, centerY, insideRadius, angle + arc, angle, true);
      ctx.stroke();
      ctx.fill();
      ctx.save();
      ctx.shadowOffsetX = -1;
      ctx.shadowOffsetY = -1;
      ctx.shadowBlur    = 0;
      ctx.shadowColor   = 'rgb(220,220,220)';
      // eslint-disable-next-line max-len
      ctx.translate(centerX + Math.cos(angle + arc / 2) * textRadius, centerY + Math.sin(angle + arc / 2) * textRadius);
      ctx.rotate(angle + arc / 2 );
      ctx.fillStyle = 'black';
      const text = option.title.toUpperCase();
      const measuredText: any = ctx.measureText(text);
      const atX = - measuredText.width / 2;
      //const fontHeight = measuredText.fontBoundingBoxAscent + measuredText.fontBoundingBoxDescent;
      const actualHeight = measuredText.actualBoundingBoxAscent + measuredText.actualBoundingBoxDescent;
      const atY = actualHeight / 2;
      ctx.fillText(text, atX, atY);
      ctx.restore();
    });

    ctx.strokeStyle = `rgba(${this.arcColor[0]}, ${this.arcColor[1]}, ${this.arcColor[2]}, 0.9)`;
    ctx.lineWidth = 20;
    ctx.beginPath();
    ctx.shadowOffsetX = -5;
    ctx.shadowOffsetY = -5;
    ctx.shadowBlur    = 20;
    ctx.shadowColor   = 'rgb(0, 0, 0)';
    ctx.arc(centerX, centerY, outsideRadius - ctx.lineWidth / 2 ,0, 2 * Math.PI);
    ctx.stroke();



    if(this.pearlImage.complete){
      options.forEach( (_, idx) => {
        const angle = this.canvasStartAngle + idx * arc;
        ctx.save();
        // eslint-disable-next-line max-len
        ctx.translate(centerX + Math.cos(angle + arc / 2) * textRadius, centerY + Math.sin(angle + arc / 2) * textRadius);
        ctx.rotate(angle + arc / 2 );
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        ctx.shadowBlur    = 14;
        ctx.shadowColor   = 'rgb(255, 255, 255)';
        //ctx.drawImage(this.pearlImage, (this.centered ? 0 : centerX) + maxTextWidth / 2 + 8, -7, 15, 15);
        ctx.beginPath();
        ctx.fillStyle= 'rgb(255, 255, 255)';
        ctx.arc((this.centered ? 0 : centerX) + maxTextWidth / 2 + ctx.lineWidth - 5, 0, 5, 0, 2 * Math.PI);
        ctx.fill();
        ctx.restore();
      });
    }

    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur    = 10;
    ctx.shadowColor   = 'rgb(0, 0, 0)';
    ctx.beginPath();
    ctx.fillStyle= this.colorAnchor;
    ctx.arc(centerX, centerY, 6, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.fillStyle= this.colorAnchor;
    ctx.moveTo(centerX + outsideRadius + ctx.lineWidth / 2 - 5, centerY - 20);
    ctx.lineTo(centerX + outsideRadius + ctx.lineWidth / 2 - 5, centerY + 20);
    ctx.lineTo(centerX + outsideRadius + ctx.lineWidth / 2 + 5, centerY + 20);
    ctx.lineTo(centerX + outsideRadius + ctx.lineWidth / 2 + 5, centerY - 20);
    ctx.fill();
    ctx.beginPath();
    ctx.fillStyle= this.colorAnchor;
    ctx.moveTo(centerX + outsideRadius - ctx.lineWidth - 2, centerY);
    ctx.quadraticCurveTo(centerX + outsideRadius, centerY - 10, centerX + outsideRadius + ctx.lineWidth / 2, centerY);
    ctx.quadraticCurveTo(centerX + outsideRadius, centerY + 10, centerX + outsideRadius - ctx.lineWidth - 2, centerY);
    ctx.fill();

    ctx.restore();

  }
  easeOut(t: number, b: number, c: number, d: number) {
    const ts = (t/=d)*t;
    const tc = ts*t;
    return b+c*(tc + -3*ts + 3*t);
  }

  stopRotateWheel(): void {
    clearTimeout(this.spinTimeout);
    this.spinTimeout = null;
  }
  rotateWheel() {
    this.spinTime += 30;
    if(this.spinTime > this.spinTimeTotal) {
      this.spinTime = this.spinTimeTotal;
      this.spinTimeChange.emit(this.spinTime);
      this.stopRotateWheel();
      return;
    }
    this.spinTimeChange.emit(this.spinTime);
    const spinAngle = this.spinAngleStart - this.easeOut(this.spinTime, 0, this.spinAngleStart, this.spinTimeTotal);
    this.canvasStartAngle += (spinAngle * Math.PI / 180.0);
    this.draw();
    this.spinTimeout = setTimeout(() => this.rotateWheel(), 30);
  }
  spin(angle?: number) {
    if(this.spinTimeout !== null) { return; }
    if(angle && angle > 200) {
      angle = 200;
    }
    if( angle ){angle = angle * 20 / 200;}
    this.spinAngleStart = angle || Math.random() * 10 + 10;
    this.spinTime = 0;
    this.spinTimeChange.emit(this.spinTime);
    // eslint-disable-next-line max-len
    this.spinTimeTotal = Math.floor(Math.random() * (this.rangeIntervalTime.upper - this.rangeIntervalTime.lower + 1) + this.rangeIntervalTime.lower) * 1000;
    this.spinTimeTotalChange.emit(this.spinTimeTotal);
    this.rotateWheel();
  }

}
