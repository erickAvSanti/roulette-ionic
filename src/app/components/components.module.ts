import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrawRouletteComponent } from './draw-roulette/draw-roulette.component';
import { ColorPickerComponent } from './color-picker/color-picker.component';



@NgModule({
  declarations: [DrawRouletteComponent, ColorPickerComponent],
  imports: [
    CommonModule
  ],
  exports: [
    DrawRouletteComponent,
    ColorPickerComponent,
  ]
})
export class ComponentsModule { }
