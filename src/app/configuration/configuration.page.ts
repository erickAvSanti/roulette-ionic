import { Component, OnInit, ViewChild } from '@angular/core';
import { IonToggle } from '@ionic/angular';
import { fromEvent } from 'rxjs';
import { DataService } from '../services/data.service';
import { LanguageService } from '../services/language.service';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

  @ViewChild('toggleTheme', { read: IonToggle }) toggleTheme: IonToggle;

  constructor(
    private themeService: ThemeService,
    private dataService: DataService,
    public languageService: LanguageService,
  ) {
  }

  ngOnInit() {

  }
  ionViewDidEnter(): void {
    this.toggleTheme.checked = this.dataService.darkModeEnabled;
  }
  toggleThemeChange(evt: CustomEvent): void {
    this.themeService.toggleThemeChange(evt);
  }

}
