import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IonRange, IonToggle, ModalController } from '@ionic/angular';
import { fromEvent } from 'rxjs';
import { ColorPickerComponent } from 'src/app/components/color-picker/color-picker.component';
import { Roulette } from 'src/app/interfaces/roulette';
import { DataService } from 'src/app/services/data.service';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss'],
})
export class ConfigComponent implements OnInit, AfterViewInit {

  @Input() minValue = 1;
  @Input() maxValue = 30;
  @Input() rangeIntervalTime = {lower: 1, upper: 10};

  @Output() interleavedColors = new EventEmitter();

  @ViewChild('periodTime', {read: IonRange}) periodTime: IonRange;
  @ViewChild('interleavedColor1', {read: ColorPickerComponent}) interleavedColor1: ColorPickerComponent;
  @ViewChild('interleavedColor2', {read: ColorPickerComponent}) interleavedColor2: ColorPickerComponent;
  @ViewChild('arcColor', {read: ColorPickerComponent}) arcColor: ColorPickerComponent;
  @ViewChild('useInterleavedColorsEl', {read: IonToggle}) useInterleavedColorsEl: IonToggle;

  interleavedColor1Value = 'rgb(0, 0, 0)';
  interleavedColor2Value = 'rgb(0, 0, 0)';
  arcColorValue = [0, 0, 0, 0];
  useInterleavedColors = false;
  roulette: Roulette;
  constructor(
    public modalController: ModalController,
    private dataService: DataService,
    private languageService: LanguageService,
  ) { }
  ngAfterViewInit(): void {
    this.periodTime.min = this.minValue;
    this.periodTime.max = this.maxValue;
    this.periodTime.value = {lower: this.rangeIntervalTime.lower, upper: this.rangeIntervalTime.upper};
  }

  ngOnInit(): void {
    this.modalController.getTop().then(
      modal => {
        fromEvent(modal, 'ionModalDidPresent').subscribe(
          _ => {
            this.interleavedColor1.drawUI();
            this.interleavedColor2.drawUI();
            this.arcColor.drawUI();

            this.useInterleavedColors = this.dataService.rouletteUseInterleavedColors;
            console.log('this.useInterleavedColors', this.useInterleavedColors);
            // this.useInterleavedColorsEl.checked = this.useInterleavedColors;
          }
        );
      }
    );
    const rangeIntervalTime = this.dataService.rouletteRangeIntervalTime;
    if(rangeIntervalTime){ this.rangeIntervalTime = rangeIntervalTime; }
    const rouletteInterleavedColors = this.dataService.rouletteInterleavedColors;
    if(rouletteInterleavedColors){
      this.interleavedColor1Value = rouletteInterleavedColors[0];
      this.interleavedColor2Value = rouletteInterleavedColors[1];
    }
    const rouletteArcColor = this.dataService.rouletteArcColor;
    if(rouletteArcColor){ this.arcColorValue = rouletteArcColor; }
  }
  get arcColorValueStr(): string{
    return `rgb(${this.arcColorValue[0]}, ${this.arcColorValue[1]}, ${this.arcColorValue[2]})`;
  }

  dismiss() {
    const interleavedColors = [this.interleavedColor1Value, this.interleavedColor2Value];
    this.modalController.dismiss({
      rangeIntervalTime: this.rangeIntervalTime,
      useInterleavedColors: this.useInterleavedColors,
      interleavedColors,
      arcColor: this.arcColorValue,
    });
  }
  periodTimeChange(evt: CustomEvent): void {
    const value = evt.detail.value;
    this.rangeIntervalTime = {...value};
    this.dataService.rouletteRangeIntervalTime = this.rangeIntervalTime;
  }
  selectedInterleavedColor1(evt: any): void {
    this.interleavedColor1Value = `rgb(${evt[0]}, ${evt[1]}, ${evt[2]})`;
    const interleavedColors = [this.interleavedColor1Value, this.interleavedColor2Value];
    this.dataService.rouletteInterleavedColors = interleavedColors;
    this.interleavedColors.emit(interleavedColors);
  }
  selectedInterleavedColor2(evt: any): void {
    this.interleavedColor2Value = `rgb(${evt[0]}, ${evt[1]}, ${evt[2]})`;
    const interleavedColors = [this.interleavedColor1Value, this.interleavedColor2Value];
    this.dataService.rouletteInterleavedColors = interleavedColors;
    this.interleavedColors.emit(interleavedColors);
  }
  selectedArcColorValue(evt: any): void {
    this.arcColorValue = [evt[0], evt[1], evt[2], evt[3]];
    this.dataService.rouletteArcColor = this.arcColorValue;
  }
  useInterleavedColorsChange(evt: CustomEvent): void {
    this.useInterleavedColors = evt.detail.checked;
    this.dataService.rouletteUseInterleavedColors = this.useInterleavedColors;
  }

}
