import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouletteDrawingSpacePageRoutingModule } from './roulette-drawing-space-routing.module';

import { RouletteDrawingSpacePage } from './roulette-drawing-space.page';
import { ComponentsModule } from '../components/components.module';
import { ViewItemsComponent } from './view-items/view-items.component';
import { ConfigComponent } from './config/config.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouletteDrawingSpacePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [RouletteDrawingSpacePage, ViewItemsComponent, ConfigComponent]
})
export class RouletteDrawingSpacePageModule {}
