import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouletteDrawingSpacePage } from './roulette-drawing-space.page';

const routes: Routes = [
  {
    path: ':id',
    component: RouletteDrawingSpacePage
  },
  {
    path: '',
    redirectTo: '/roulettes',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouletteDrawingSpacePageRoutingModule {}
