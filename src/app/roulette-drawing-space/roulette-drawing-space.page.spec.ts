import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RouletteDrawingSpacePage } from './roulette-drawing-space.page';

describe('RouletteDrawingSpacePage', () => {
  let component: RouletteDrawingSpacePage;
  let fixture: ComponentFixture<RouletteDrawingSpacePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RouletteDrawingSpacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RouletteDrawingSpacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
