import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { timer } from 'rxjs';
import { Roulette } from '../interfaces/roulette';
import { DataService } from '../services/data.service';
import { LanguageService } from '../services/language.service';
import { ConfigComponent } from './config/config.component';
import { ViewItemsComponent } from './view-items/view-items.component';

@Component({
  selector: 'app-roulette-drawing-space',
  templateUrl: './roulette-drawing-space.page.html',
  styleUrls: ['./roulette-drawing-space.page.scss'],
})
export class RouletteDrawingSpacePage implements OnInit {

  roulette: Roulette;
  compTitle = '';
  spinTime = 0;
  spinTimeTotal = 0;

  centeredRoulette = true;
  fillFrameRoulette = true;

  rangeIntervalTime = {lower: 3, upper: 10};
  interleavedColors = ['rgb(229, 142, 60)', 'rgb(229, 209, 60)'];
  useInterleavedColors = false;
  arcColor = [0, 0, 0, 0];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    public toastController: ToastController,
    public modalController: ModalController,
    private languageService: LanguageService,
  ) {
    this.useInterleavedColors = this.dataService.rouletteUseInterleavedColors;
    const rangeIntervalTime = this.dataService.rouletteRangeIntervalTime;
    if(rangeIntervalTime){
      this.rangeIntervalTime = rangeIntervalTime;
    }
    const arcColor = this.dataService.rouletteArcColor;
    if(arcColor){
      this.arcColor = arcColor;
    }
    this.route.paramMap.subscribe(
      ( _: any ) => {
        console.log(' _ ', _);
        const idx = parseInt(_.params.id, 10);
        console.log(`idx = ${idx}`);
        if(!idx){
          this.showToast(this.languageService.getText('roulette ID malformed'));
          timer(600).subscribe(
            () => this.router.navigateByUrl('/')
          );
        }else{
          const roulette = this.dataService.getRoulette(idx);
          if(!roulette){
            this.showToast(this.languageService.getText('roulette not found'));
            timer(600).subscribe(
              () => this.router.navigateByUrl('/')
            );
          }else{
            this.compTitle = roulette.title;
            this.roulette = roulette;
          }
        }
      });
  }

  ngOnInit() {
  }
  async showToast(message: string): Promise<any> {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }
  view(): void {
    this.router.navigateByUrl(`/roulettes/${this.roulette.id}`);
  }
  async viewItems(): Promise<any> {
    const modal = await this.modalController.create({
      component: ViewItemsComponent,
      cssClass: '',
      componentProps: {
        roulette: this.roulette,
      }
    });
    return await modal.present();
  }
  async changeConfig(): Promise<any> {
    const modal = await this.modalController.create({
      component: ConfigComponent,
      cssClass: '',
      componentProps: {
        roulette: this.roulette,
      },
    });
    modal.onWillDismiss().then(
      (result: any) => {
        console.log(`changePeriodTime - onWillDismiss`,result);
        if(result.data){
          this.rangeIntervalTime = {...result.data.rangeIntervalTime};
          this.interleavedColors = [...result.data.interleavedColors];
          this.useInterleavedColors = result.data.useInterleavedColors;
          this.arcColor = result.data.arcColor;
        }
      }
    );
    return await modal.present();
  }
  spinTimeChange(spinTime: number): void {
    this.spinTime = spinTime;
  }
  spinTimeTotalChange(spinTimeTotal: number): void {
    this.spinTimeTotal = spinTimeTotal;
  }
  rounded2(val: number): string {
    return val.toFixed(2);
  }

}
