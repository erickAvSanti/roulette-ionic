import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Roulette } from 'src/app/interfaces/roulette';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-view-items',
  templateUrl: './view-items.component.html',
  styleUrls: ['./view-items.component.scss'],
})
export class ViewItemsComponent implements OnInit, AfterViewInit {

  roulette: Roulette;
  optionsList = [];

  constructor(
    public modalController: ModalController,
    private languageService: LanguageService,
  ) { }
  ngAfterViewInit(): void {
  }

  ngOnInit() {

    this.optionsList = this.roulette.options;
  }

  getColorStr(color: any): string {
    return `rgb(${color.r},${color.g},${color.b})`;
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
