import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormRoulettePage } from './form-roulette.page';

const routes: Routes = [
  {
    path: '',
    component: FormRoulettePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormRoulettePageRoutingModule {}
