import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, IonContent, ModalController, Platform } from '@ionic/angular';
import { fromEvent } from 'rxjs';
import { ColorPickerComponent } from 'src/app/components/color-picker/color-picker.component';
import { RouletteOption } from 'src/app/interfaces/roulette-option';
import { DataService } from 'src/app/services/data.service';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-roulette-item-option',
  templateUrl: './roulette-item-option.component.html',
  styleUrls: ['./roulette-item-option.component.scss'],
})
export class RouletteItemOptionComponent implements OnInit, AfterViewInit{
  @ViewChild('optionColor', {read: ColorPickerComponent}) optionColor: ColorPickerComponent;
  @ViewChild(IonContent, { static: true}) ionContent: IonContent;
  @Input() option: RouletteOption;
  colors = [];
  selectedColor: Uint8ClampedArray = new Uint8ClampedArray(4);


  form: FormGroup;
  compTitle = this.languageService.getText('Add Option');
  constructor(
    public modalController: ModalController,
    public platform: Platform,
    public alertController: AlertController,
    public fb: FormBuilder,
    private languageService: LanguageService,
    private dataService: DataService,
  ) {

    this.form = this.fb.group({
      id: [null, ],
      title: [ '', { validators: [Validators.required, Validators.maxLength(35), Validators.minLength(1)] }],
      color: [ {r: this.ramdomHex, g: this.ramdomHex, b: this.ramdomHex}, { validators: [Validators.required] }]
    });
  }
  ngOnInit(): void {
    if(this.option){
      this.compTitle = this.languageService.getText('Edit Option');
      this.id.setValue(this.option.id);
      this.title.setValue(this.option.title);
      this.color.setValue(this.option.color);
    }
    this.modalController.getTop().then(
      modal => {
        fromEvent(modal, 'ionModalDidPresent').subscribe(
          _ => {
            this.optionColor.drawUI();
          }
        );
      }
    );
  }
  ngAfterViewInit(): void {
  }

  get ramdomHex(): number {
    return parseInt(`${Math.random() * 255}`, 10);
  }

  get color() {
    return this.form.get('color');
  }
  get title() {
    return this.form.get('title');
  }
  get id(){
    return this.form.controls.id;
  }

  get selectedColorStr(): string {
    const selectedColor = this.color.value;
    return `rgb(${selectedColor.r},${selectedColor.g},${selectedColor.b})`;
  }

  confirm(): void {
    if(!this.id.value){
      this.id.setValue(this.dataService.getRandomId());
    }
    this.modalController.dismiss(this.form.value);
  }

  async save(): Promise<any> {
    if(this.form.invalid) {return null;}
    const alert = await this.alertController.create({
      cssClass: 'alert-save',
      header: `${this.languageService.getText('Alert')}!`,
      message: `${this.languageService.getText('Apply changes')}?`,
      buttons: [
        {
          text: this.languageService.getText('Cancel'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}
        }, {
          text: this.languageService.getText('Confirm'),
          handler: () => {
            this.confirm();
          }
        }
      ]
    });

    await alert.present();
  }
  goBack(): void {
    this.modalController.dismiss();
  }
  selectedOptionColor(evt: Uint8ClampedArray): void {
    this.selectedColor = evt;
    this.color.setValue({r: evt[0], g: evt[1], b: evt[2]});
  }

}
