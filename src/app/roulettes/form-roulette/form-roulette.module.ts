import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormRoulettePageRoutingModule } from './form-roulette-routing.module';

import { FormRoulettePage } from './form-roulette.page';
import { RouletteItemOptionComponent } from './roulette-item-option/roulette-item-option.component';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    FormRoulettePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [FormRoulettePage, RouletteItemOptionComponent]
})
export class FormRoulettePageModule {}
