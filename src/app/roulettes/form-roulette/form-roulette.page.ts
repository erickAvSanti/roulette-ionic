import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, IonList, IonReorderGroup, ModalController } from '@ionic/angular';
import { ItemReorderEventDetail } from '@ionic/core';
import { timer } from 'rxjs';
import { Roulette } from 'src/app/interfaces/roulette';
import { RouletteOption } from 'src/app/interfaces/roulette-option';
import { DataService } from 'src/app/services/data.service';
import { LanguageService } from 'src/app/services/language.service';
import { RouletteItemOptionComponent } from './roulette-item-option/roulette-item-option.component';

@Component({
  selector: 'app-form-roulette',
  templateUrl: './form-roulette.page.html',
  styleUrls: ['./form-roulette.page.scss'],
})
export class FormRoulettePage implements OnInit {

  @ViewChild(IonReorderGroup, {read: IonReorderGroup}) ionReorderGroup: IonReorderGroup;
  @ViewChild('mainList', { read: IonList }) mainList: IonList;

  form: FormGroup;
  pageTitle = '';

  roulette: Roulette;

  constructor(
    public modalController: ModalController,
    public alertController: AlertController,
    public fb: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private languageService: LanguageService,
  ) {

    this.route.paramMap.subscribe(
      ( _: any ) => {
        console.log(' _ ', _);
        const idx = parseInt(_.params.id, 10);
        console.log(`idx = ${idx}`);
        if(!idx){
          this.form.get('id').setValue(null);
          this.pageTitle = this.languageService.getText('New Roulette');
        }else{
          this.form.get('id').setValue(idx);
          this.roulette = this.dataService.getRoulette(idx);
          if ( this.roulette ) {
            this.pageTitle = this.roulette.title;
            timer(100).subscribe(
              () => {
                this.title.setValue(this.roulette.title);
                this.optionValues = this.roulette.options;
              }
            );
          }else{
            timer(10).subscribe(
              () => {
                this.router.navigate(['../new'], { relativeTo: this.route });
              }
            );
          }
        }
      }
    );

    this.form = this.fb.group({
      id: [null, []],
      title: [ '', { validators: [Validators.required, Validators.minLength(1), Validators.maxLength(40)] }],
      options: this.fb.array([])
    });
  }

  get id() {
    return this.form.get('id');
  }
  get title() {
    return this.form.get('title');
  }
  get options() {
    return this.form.get('options') as FormArray;
  }

  set optionValues(options: Array<RouletteOption>) {
    this.form.setControl('options', new FormArray(options.map( e => new FormControl(e))));
  }

  ngOnInit() {
  }
  toggleReorder(): void {
    this.ionReorderGroup.disabled = !this.ionReorderGroup.disabled;
  }
  doReorder(ev: CustomEvent<ItemReorderEventDetail>) {
    console.log('Dragged from index', ev.detail.from, 'to', ev.detail.to);
    ev.detail.complete();
    timer(2).subscribe(
      () => {

        const from = this.options.controls[ev.detail.from];
        const to = this.options.controls[ev.detail.to];
        this.options.controls[ev.detail.from] = to;
        this.options.controls[ev.detail.to] = from;
        this.roulette.options = this.options.getRawValue();
        this.roulette = {...this.roulette};

      }
    );
  }
  async addOption(): Promise<any> {
    const modal = await this.modalController.create({
      component: RouletteItemOptionComponent,
      cssClass: 'roulette-item-option',
      swipeToClose: true,
    });
    modal.onWillDismiss().then(
      ({data: option}: {data: RouletteOption}) => {
        if(option){
          this.options.push(this.fb.control(option));
          console.log(this.options.getRawValue());
          this.roulette.options = this.options.getRawValue();
          this.roulette = {...this.roulette};
        }
      }
    );
    return await modal.present();
  }

  async save(): Promise<any> {
    const alert = await this.alertController.create({
      cssClass: 'alert-save',
      header: `${this.languageService.getText('Alert')}!`,
      message: `${this.languageService.getText('Save changes')}?`,
      buttons: [
        {
          text: this.languageService.getText('Cancel'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (_) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: this.languageService.getText('Confirm'),
          handler: () => {
            this.confirm();
          }
        }
      ]
    });

    await alert.present();
  }
  confirm(): void {
    // eslint-disable-next-line max-len
    const id = this.id.value;
    console.log(`confirm Id = ${id}`);
    const formRaw = this.form.getRawValue();
    console.log(`formRaw = ${JSON.stringify(formRaw)}`);
    const roulette = !id ? this.dataService.addRoulette(this.title.value) : this.dataService.editRoulette(formRaw);
    if(id){
      this.pageTitle = this.title.value;
      this.dataService.setRouletteOptions(id, this.options.controls.map( c => c.value));
    }
    this.router.navigate([`../${roulette.id}`], { relativeTo: this.route });
  }
  goBack(): void {
    this.router.navigate([`../`], { relativeTo: this.route });
  }

  getOptionColor(val: any): string {
    return `rgb(${val.color.r}, ${val.color.g}, ${val.color.b})`;
  }
  async editOption(option: RouletteOption, idx: number): Promise<any> {

    const modal = await this.modalController.create({
      component: RouletteItemOptionComponent,
      cssClass: 'roulette-item-option',
      componentProps: {
        option,
      },
      swipeToClose: true,
    });
    modal.onWillDismiss().then(
      ({data}: { data: RouletteOption}) => {
        if(data){
          option.title = data.title;
          option.color = data.color;
          const ctrl = this.options.controls[idx];
          ctrl.setValue(option);
          this.roulette = {...this.roulette};
          this.mainList.closeSlidingItems();
        }
      }
    );
    return await modal.present();
  }
  async delOption(option: RouletteOption, idx: number): Promise<any> {
    const alert = await this.alertController.create({
      cssClass: 'alert-delete',
      header: `${this.languageService.getText('Delete')} ${option.title}?`,
      message: this.languageService.getText('You can not revert this action'),
      buttons: [
        {
          text: this.languageService.getText('Cancel'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (_) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: this.languageService.getText('Confirm'),
          handler: () => {
            this.confirmDeletionOption(option, idx);
          }
        }
      ]
    });

    await alert.present();
  }
  confirmDeletionOption(option: RouletteOption, idx: number): void {
    this.roulette.options = this.roulette.options.filter(opt => opt.id !== option.id);
    this.roulette = {...this.roulette};
    this.options.removeAt(idx, { emitEvent: true });
  }
  play(): void {
    this.router.navigateByUrl(`/roulette-drawing-space/${this.roulette.id}`);
  }
}
