import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoulettesPage } from './roulettes.page';

const routes: Routes = [
  {
    path: '',
    component: RoulettesPage
  },
  {
    path: ':id',
    loadChildren: () => import('./form-roulette/form-roulette.module').then( m => m.FormRoulettePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoulettesPageRoutingModule {}
