import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, IonList, IonReorderGroup } from '@ionic/angular';
import { ItemReorderEventDetail } from '@ionic/core';
import { Roulette } from '../interfaces/roulette';
import { DataService } from '../services/data.service';
import { LanguageService } from '../services/language.service';

@Component({
  selector: 'app-roulettes',
  templateUrl: './roulettes.page.html',
  styleUrls: ['./roulettes.page.scss'],
})
export class RoulettesPage implements OnInit {

  @ViewChild(IonReorderGroup, {read: IonReorderGroup}) ionReorderGroup: IonReorderGroup;
  @ViewChild('mainList', { read: IonList }) mainList: IonList;

  list = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService,
    private alertController: AlertController,
    private languageService: LanguageService,
  ) { }

  ngOnInit() {
  }
  ionViewDidEnter(): void {
    this.list = this.dataService.roulettes;
  }
  toggleReorder(): void {
    this.ionReorderGroup.disabled = !this.ionReorderGroup.disabled;
  }
  doReorder(ev: CustomEvent<ItemReorderEventDetail>) {
    console.log('Dragged from index', ev.detail.from, 'to', ev.detail.to);
    this.list = ev.detail.complete(this.list);
    this.dataService.roulettes = this.list;
  }
  add(): void {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
  edit(item: Roulette): void {
    this.mainList.closeSlidingItems();
    this.router.navigate([item.id], {relativeTo: this.route});
  }
  async delete(item: Roulette): Promise<any> {
    const alert = await this.alertController.create({
      cssClass: 'alert-delete',
      header: `${this.languageService.getText('Delete')} ${item.title}?`,
      message: this.languageService.getText('You can not revert this action'),
      buttons: [
        {
          text: this.languageService.getText('Cancel'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (_) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: this.languageService.getText('Confirm'),
          handler: () => {
            this.confirmDeletion(item);
          }
        }
      ]
    });

    await alert.present();
  }

  confirmDeletion(item: Roulette): void {
    const roulette = this.dataService.deleteRoulette(item.id);
    this.list = this.list.filter( e => e.id !== roulette.id);
  }
  playRoulette(roulette: Roulette): void {
    this.router.navigateByUrl(`/roulette-drawing-space/${roulette.id}`);
  }

}
