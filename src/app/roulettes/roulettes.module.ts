import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoulettesPageRoutingModule } from './roulettes-routing.module';

import { RoulettesPage } from './roulettes.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoulettesPageRoutingModule,
  ],
  declarations: [RoulettesPage]
})
export class RoulettesPageModule {}
