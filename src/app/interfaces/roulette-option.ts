export interface RouletteOption{
  id?: number;
  title: string;
  color: {r: number; g: number; b: number};
}
