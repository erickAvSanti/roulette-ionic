import { RouletteOption } from './roulette-option';

export interface Roulette{
  id: number;
  title: string;
  options: Array<RouletteOption>;
}
